package com.zz.cogo.account.api;

import com.zz.cogo.account.bo.UserLoginBo;
import com.zz.cogo.account.vo.AuthVo;
import com.zz.cogo.common.vo.CommonVo;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 提供对外开放接口
 */
public interface UserApi {

    @GetMapping("/account/queryByAuth")
    public CommonVo<AuthVo> queryByAuth(@SpringQueryMap UserLoginBo userLoginBo);

}
