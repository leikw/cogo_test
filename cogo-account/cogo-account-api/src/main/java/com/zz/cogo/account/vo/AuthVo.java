package com.zz.cogo.account.vo;

import com.zz.cogo.account.entity.User;

/**
 * @ClasName LoginVo
 * @Descripution TODO
 * @Author leikw
 * @Date 2021/11/19 9:36
 * @Version V1.0
 */
public class AuthVo {

    private String uid;

    private String userName;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public AuthVo(User user) {
        this.uid = String.valueOf(user.getId());
        this.userName =user.getLoginName();
    }

    public AuthVo() {
    }
}
