package com.zz.cogo.account.config;


import com.zz.cogo.common.rocketmq.config.RocketMqAdapter;
import com.zz.cogo.common.rocketmq.config.RocketMqConstant;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@RefreshScope
@Configuration
public class RocketMqConfig {

    @Autowired
    private RocketMqAdapter rocketMqAdapter;

    @Lazy
    @Bean(destroyMethod = "destroy")
    public RocketMQTemplate test1Template() {
        return rocketMqAdapter.getTemplateByTopicName(RocketMqConstant.TEST1_TOPIC);
    }

    @Lazy
    @Bean(destroyMethod = "destroy")
    public RocketMQTemplate test2Template() {
        return rocketMqAdapter.getTemplateByTopicName(RocketMqConstant.TEST2_TOPIC);
    }


}
