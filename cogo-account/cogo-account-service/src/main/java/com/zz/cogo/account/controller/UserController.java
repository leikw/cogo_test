package com.zz.cogo.account.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import com.zz.cogo.account.bo.UserLoginBo;
import com.zz.cogo.account.entity.User;
import com.zz.cogo.account.service.UserService;
import com.zz.cogo.account.vo.AuthVo;
import com.zz.cogo.common.rocketmq.config.RocketMqConstant;
import com.zz.cogo.common.vo.CommonVo;
import com.zz.cogo.common.vo.ResultCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 * @ClasName UserController
 * @Descripution TODO
 * @Author leikw
 * @Date 2021/11/18 17:22
 * @Version V1.0
 */
@RestController
@RequestMapping("/account")
@Api("用户操作")
public class UserController {

    @Autowired
    UserService userService;


    @Autowired
    private RocketMQTemplate test1Template;


    @GetMapping("/list")
    @ApiOperation(value = "用户列表", notes = "用户列表")
    public CommonVo<List<User>> getUsers(){
        List<User> list = userService.list();
        return new CommonVo<List<User>>(ResultCode.SUCCESS,list);
    }


    @GetMapping("/queryByAuth")
    public CommonVo<AuthVo> queryByAuth(@SpringQueryMap UserLoginBo userLoginBo){

        System.out.println(userLoginBo);
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();

        userQueryWrapper.eq("login_name",userLoginBo.getLoginName());

        User one = userService.getOne(userQueryWrapper);
        //两层加密
        userLoginBo.setPassword(DigestUtils.md5DigestAsHex(userLoginBo.getPassword().getBytes()));

        if(one==null){
            //用户名不存在
        }else if(! userLoginBo.getPassword().equals(one.getPassword())){
            //密码错误
        }else{
            //登录成功
        }

        return new CommonVo<AuthVo>(ResultCode.SUCCESS,new AuthVo(one));
    }


    //使用rocketMQ推送消息
    @GetMapping("/msgTest1")
    public void msgTest1(){
        //53098422880579584
        User byId = userService.getById(53098422880579584L);

        System.out.println("使用rocketMQ推送消息");
        // 发送消息
        SendStatus sendStatus = test1Template.syncSend( //同步发送
                RocketMqConstant.TEST1_TOPIC,  //业务主题
                new GenericMessage<>(byId) //内容
        ).getSendStatus();

        if (!Objects.equals(sendStatus, SendStatus.SEND_OK)) {
            // 消息发不出去就抛异常
            throw new RuntimeException("发送失败");
        }
    }

}
