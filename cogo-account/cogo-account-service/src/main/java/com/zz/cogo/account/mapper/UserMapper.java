package com.zz.cogo.account.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zz.cogo.account.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper extends BaseMapper<User> {

}
