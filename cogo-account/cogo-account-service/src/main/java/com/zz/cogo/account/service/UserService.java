package com.zz.cogo.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zz.cogo.account.entity.User;

public interface UserService  extends IService<User> {

    public User getUser();

}
