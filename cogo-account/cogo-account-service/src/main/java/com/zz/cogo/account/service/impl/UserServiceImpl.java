package com.zz.cogo.account.service.impl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zz.cogo.account.entity.User;
import com.zz.cogo.account.mapper.UserMapper;
import com.zz.cogo.account.service.UserService;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ClasName UserServiceImpl
 * @Descripution TODO
 * @Author leikw
 * @Date 2021/11/18 16:33
 * @Version V1.0
 */
@Service
public class UserServiceImpl  extends ServiceImpl<UserMapper, User>  implements UserService  {

    @Autowired
    UserMapper userMapper;


    @Override
    public User getUser() {
        return userMapper.selectById("53098422880579584") ;
    }
}
