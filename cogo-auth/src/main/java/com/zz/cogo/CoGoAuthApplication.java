package com.zz.cogo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
@EnableEurekaClient
@RestController
@RefreshScope // 这边的@RefreshScope注解不能少，否则即使调用 actuator/refresh，配置也不会刷新
@EnableFeignClients
public class CoGoAuthApplication {

    @GetMapping(value = "show")
    public Object show(){
        return "";
    }

    public static void main(String[] args) {
        SpringApplication.run(CoGoAuthApplication.class);
    }

}

