package com.zz.cogo.auth.client;

import com.zz.cogo.account.api.UserApi;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "cogo-account")
public interface UserClient extends UserApi {
}
