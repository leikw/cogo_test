package com.zz.cogo.auth.listener;

import com.alibaba.fastjson.JSON;
import com.zz.cogo.account.entity.User;
import com.zz.cogo.common.rocketmq.config.RocketMqConstant;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @ClasName Test1Consumer
 * @Descripution TODO
 * @Author leikw
 * @Date 2021/11/20 10:53
 * @Version V1.0
 */
@Component
@RocketMQMessageListener(topic = RocketMqConstant.TEST1_TOPIC,consumerGroup = RocketMqConstant.TEST1_TOPIC)
public class Test1Consumer  implements RocketMQListener<User> {

    @Override
    public void onMessage(User message) {
        System.out.println("订单回调开始... message: " + JSON.toJSONString(message));

       //业务操作
    }

}
