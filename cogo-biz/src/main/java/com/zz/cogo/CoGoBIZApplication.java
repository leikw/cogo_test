package com.zz.cogo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@RestController
@RefreshScope // 这边的@RefreshScope注解不能少，否则即使调用 actuator/refresh，配置也不会刷新
public class CoGoBIZApplication {

    @Autowired
    private GitConfig gitConfig;

    @Autowired
    ConfigurableApplicationContext applicationContext;

    //通过value注解读取配置信息
    @Value("${data.env}")
    private String config1;

    @GetMapping("/configs")
    public String getConfigs(){
        return applicationContext.getEnvironment().getProperty("data.env");
    }

    @GetMapping(value = "show")
    public Object show(){
        return gitConfig;
    }



    public static void main(String[] args) {
        SpringApplication.run(CoGoBIZApplication.class);
    }

}

