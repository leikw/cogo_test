package com.zz.cogo;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class GitConfig {

    @Value("${data.env}")
    private String env;
}
