package com.zz.cogo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@RestController
@RefreshScope // 这边的@RefreshScope注解不能少，否则即使调用 actuator/refresh，配置也不会刷新
public class CoGoGetWayApplication {

    public static void main(String[] args) {
        SpringApplication.run(CoGoGetWayApplication.class);
    }

}

