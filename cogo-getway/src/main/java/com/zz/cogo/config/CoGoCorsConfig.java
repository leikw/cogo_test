package com.zz.cogo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsConfigurationSource;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * @author li
 * @time:2018/8/7
 * 处理跨域请求的过滤器
 */
@Configuration
public class CoGoCorsConfig {
    @Bean
    public CorsWebFilter corsFilter() {
        //1.添加CORS配置信息
        CorsConfiguration corsConfiguration = new CorsConfiguration();

        //1) 允许的域,不要写*，否则cookie就无法使用了
//        config.addAllowedOrigin("*");
        corsConfiguration.addAllowedOrigin("http://test.cogo.com/");

        //2) 是否发送Cookie信息
        corsConfiguration.setAllowCredentials(true);
        //3) 允许的请求方式
        corsConfiguration.addAllowedMethod("*");
        // 4）允许的头信息
        corsConfiguration.addAllowedHeader("*");

        //2.添加映射路径，拦截一切请求
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", corsConfiguration);

        //3.返回新的CorsFilter.
        return new CorsWebFilter(source);
    }
}
