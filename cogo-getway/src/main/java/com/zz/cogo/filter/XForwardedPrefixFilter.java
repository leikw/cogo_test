package com.zz.cogo.filter;

import com.zz.cogo.config.SwaggerResourceConfig;
import org.springframework.cloud.gateway.filter.headers.HttpHeadersFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

@Component
public class XForwardedPrefixFilter implements HttpHeadersFilter, Ordered {

    @Override
    public HttpHeaders filter(HttpHeaders input, ServerWebExchange exchange) {
        String path = exchange.getRequest().getURI().getPath();
        if(path.endsWith(SwaggerResourceConfig.API_URI)
        || path.endsWith("/v2/api-docs-ext")){
            input.set("X-Forwarded-Prefix", "/");
        }
        return input;
    }

    @Override
    public int getOrder() {
        return 1;
    }
}
