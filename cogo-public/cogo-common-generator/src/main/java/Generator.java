import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

/**
 * @ClasName Generator
 * @Descripution TODO
 * @Author leikw
 * @Date 2021/12/1 15:14
 * @Version V1.0
 */
public class Generator {

        public static void main(String[] args) {

            // 1、创建代码生成器
            AutoGenerator mpg = new AutoGenerator();

            // 2、全局配置
            GlobalConfig gc = new GlobalConfig();
//            String projectPath = System.getProperty("user.dir");
            gc.setOutputDir("E:\\work\\a_idea_work\\新建文件夹2021-11-18\\cogo-parent\\cogo-public\\cogo-common-generator\\src\\main\\src");
            gc.setAuthor("leikw");
            gc.setOpen(false); //生成后是否打开资源管理器
            gc.setFileOverride(true); //重新生成时文件是否覆盖
            gc.setServiceName("%sService"); //去掉Service接口的首字母I
            //主键策略
            // AUTO(0),			//数据库ID自增
            //ID_WORKER(3),		//数值类型全局唯一ID，内容为空自动填充（默认配置）
            gc.setIdType(IdType.ID_WORKER_STR);
            gc.setDateType(DateType.ONLY_DATE);//定义生成的实体类中日期类型
            gc.setSwagger2(true);//开启Swagger2模式

            mpg.setGlobalConfig(gc);

            // 3、数据源配置
            DataSourceConfig dsc = new DataSourceConfig();
            dsc.setUrl("jdbc:mysql://121.4.248.202:3308/zz-user?serverTimezone=GMT%2B8");
            dsc.setDriverName("com.mysql.cj.jdbc.Driver");
            dsc.setUsername("root");
            dsc.setPassword("cogo@test123");
            dsc.setDbType(DbType.MYSQL);
            mpg.setDataSource(dsc);

            // 4、包配置
            PackageConfig pc = new PackageConfig();
            pc.setModuleName(null); //模块名
            pc.setParent("com.zz.user");
            pc.setController("controller");
            pc.setEntity("entity");
            pc.setService("service");
            pc.setMapper("mapper");
            mpg.setPackageInfo(pc);

            // 5、策略配置
            StrategyConfig strategy = new StrategyConfig();
            strategy.setInclude("sys_user");//对那一张表生成代码
            strategy.setNaming(NamingStrategy.underline_to_camel);//数据库表映射到实体的命名策略
            strategy.setTablePrefix(pc.getModuleName() + "_"); //生成实体时去掉表前缀

            strategy.setColumnNaming(NamingStrategy.underline_to_camel);//数据库表字段映射到实体的命名策略
            strategy.setEntityLombokModel(true); // lombok 模型 @Accessors(chain = true) setter链式操作

            strategy.setRestControllerStyle(true); //restful api风格控制器
            strategy.setControllerMappingHyphenStyle(true); //url中驼峰转连字符

            mpg.setStrategy(strategy);

            // 6、执行
            mpg.execute();
        }

}
