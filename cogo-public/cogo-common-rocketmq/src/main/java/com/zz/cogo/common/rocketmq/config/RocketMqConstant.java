package com.zz.cogo.common.rocketmq.config;

public class RocketMqConstant {

    // 延迟消息参数 1s 5s 10s 30s 1m 2m 3m 4m 5m 6m 7m 8m 9m 10m 20m 30m 1h 2h (1-18)

    /**
     * 延迟时间，16实际上为30分钟
     */
    public static final int DELAY_LEVEL = 16;

    /**
     * 默认发送消息超时时间
     */
    public static final long TIMEOUT = 3000;


    /**
     * 测试topic
     */
    public static final String TEST1_TOPIC = "test1-topic";

    /**
     * 测试topic
     */
    public static final String TEST2_TOPIC = "test2-topic";





}
