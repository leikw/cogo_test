package com.zz.cogo.common.vo;

public class CommonVo<T> {

    /**
     * 业务上的成功或失败
     */
    private String result;

    /**
     * 返回信息
     */
    private String message;

    /**
     * 返回泛型数据，自定义类型
     */
    private T content;

    @Override
    public String toString() {
        return "CommonVo{" +
                "result='" + result + '\'' +
                ", message='" + message + '\'' +
                ", content=" + content +
                '}';
    }

    public CommonVo(ResultCode resultCode,T content) {
        this.result = resultCode.getCode();
        this.message = resultCode.getMsg();
        this.content = content;
    }

    public CommonVo(ResultCode resultCode) {
        this.result = resultCode.getCode();
        this.message = resultCode.getMsg();
    }

    public CommonVo() {
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }
}
