package com.zz.cogo.common.vo;

public enum ResultCode {
    SUCCESS("200","success"), UNAUTH("401","unauth"),

    LOGIN_EXCEPTION("10010","login_exception"), NO_NULL("10050","no_null");

    private String code;


    private String msg;

    private ResultCode(String code) {
        this.code = code;
    }

    private ResultCode(String code,String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }
    public String getMsg() {
        return msg;
    }
}
