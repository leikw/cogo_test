package com.zz.cogo.search;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

/**
 * <p>
 * 启动类
 * </p>
 *
 * @author yangkai.shen
 * @date Created in 2018-10-27 22:52
 */
@SpringBootApplication
public class SpringBootSearchApplication {

//    @PostConstruct
//    public void init() {
//        // 解决netty启动冲突的问题（主要体现在启动redis和elasticsearch）
//        // 可以看Netty4Util.setAvailableProcessors(..)
//        System.setProperty("es.set.netty.runtime.available.processors", "false");
//    }

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSearchApplication.class, args);
    }

}
