package com.zz.cogo.search.constants;

public interface EsConsts {
    /**
     * 索引名称
     */
    String INDEX_NAME = "person";

    /**
     * 类型名称
     */
    String TYPE_NAME = "person";
}
