package com.zz.cogo.search.repository;

import com.zz.cogo.search.model.Person;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface PersonRepository extends ElasticsearchRepository<Person, Long> {

    List<Person> findByAgeBetween(Integer min, Integer max);

}
