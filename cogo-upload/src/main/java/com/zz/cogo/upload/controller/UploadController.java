package com.zz.cogo.upload.controller;

import com.zz.cogo.upload.service.FileDfsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@RestController
@RequestMapping("/upload")
@Api("文件上传")
public class UploadController {

	@Autowired
	private FileDfsService fileDfsService;

	/**
	 * 文件上传
	 */
	@ApiOperation(value = "文件上传", notes = "文件上传")
//	@ApiImplicitParams({@ApiImplicitParam(name = "file", value = "指定文件", required = true)})
	@RequestMapping(value = "/uploadFile", headers = "content-type=multipart/form-data", method = RequestMethod.POST)
	public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file) {
		String result;
		try {
			String path = fileDfsService.upload(file);
			if (!StringUtils.isEmpty(path)) {
				result = path;
			} else {
				result = "上传失败";
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = "服务异常";
		}
		return ResponseEntity.ok(result);
	}

	/**
	 * 文件删除
	 */
	@RequestMapping(value = "/deleteByPath", method = RequestMethod.GET)
	public ResponseEntity<String> deleteByPath() {
//		String filePathName = "group1/M00/00/00/wKhIgl0n4AKABxQEABhlMYw_3Lo825.png";
//		fileDfsService.deleteFile(filePathName);
		return ResponseEntity.ok("SUCCESS");
	}
}
